import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common import keys
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


def Chrome_driver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")

    driver = webdriver.Chrome("/snap/bin/chromium.chromedriver", chrome_options=chrome_options)

    driver.maximize_window()
    return driver


def login(driver):
    driver.get("https://vi-vn.facebook.com")

    email = driver.find_element_by_id('email')
    email.send_keys("0919222420")
    time.sleep(1)

    password = driver.find_element_by_id('pass')
    password.send_keys("bongneo04032001")
    time.sleep(1)

    btnLogin = driver.find_element_by_name('login')
    btnLogin.send_keys(Keys.ENTER)
    time.sleep(5)


def search(driver):
    search_F = driver.find_element_by_xpath(
        "//input[@aria-label='Tìm kiếm trên Facebook']")
    search_F.click()
    search_F.send_keys('Tâm', Keys.SPACE, 'Nguyễn')
    search_F.send_keys(Keys.ENTER)
    time.sleep(3)

    findfriend1 = driver.find_element_by_xpath(
        "//a[@aria-label='Tâm Nguyễn']")
    findfriend1.click()
    time.sleep(2)


def like(driver):
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(4)

    likes = driver.find_elements_by_xpath(
        "//div[@class='tvfksri0 ozuftl9m jmbispl3 olo4ujb6']//div[@aria-label='Thích']")
    time.sleep(5)

    for i in range(0, 6):
        actions = ActionChains(driver)
        actions.move_to_element(likes[i])
        actions.click()
        actions.perform()
        time.sleep(3)


def profile_friend(driver):
    driver.get("https://www.facebook.com/Tamnguyen43")
    time.sleep(2)


def main():
    a = input(
        "00:login &like \n 01: go to profile\n")

    driver = Chrome_driver()

    if int(a) == 0:
        login(driver)
        search(driver)
        like(driver)
    else:
        profile_friend(driver)

    driver.close()


if __name__ == "__main__":
    main()
