from GGsearch import google_bot
import json

if __name__== '__main__':
    with open('config.json') as file:
        comfig=json.load(file)

    driver = comfig['driver']
    url=comfig['url']

    gg = google_bot(url)
    gg.runner()

