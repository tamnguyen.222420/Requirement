from builtins import str

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import pandas as pd
class google_bot:
    driver = webdriver.Chrome("/snap/bin/chromium.chromedriver")
    driver.maximize_window()
    def __init__(self,url):
        self.driver.get(url)

    def List_title(self):
        list_title= []
        title = self.driver.find_elements_by_xpath("//div[@class='yuRUbf']//h3[@class='LC20lb DKV0Md']")
        for i in range(0,int(len(title))):
            list_title.append(title[i].text)
        return list_title

    def List_Links(self):
        list_link = []
        link = self.driver.find_elements_by_xpath("//div[@class='yuRUbf']/a")
        time.sleep(1)
        for i in range(0,int(len(link))):
            list_link.append(link[i].get_attribute("href"))
        return list_link
    def get_result(self,stop):
        list1=[]
        lis2=[]

        list_links = self.List_Links()
        list_titles = self.List_title()

        start = 0
        for i in range(0,len(list_titles)):
            if(start!=stop):
                if(list_titles!=""):
                    list1.append(list_titles[i])
                    lis2.append(list_links[i])
                    start= start+1
            else:
                break
        return list1,lis2

    def runner(self):
        stop = 6
        searchbox=self.driver.find_element_by_xpath("//input[@title='Tìm kiếm']")
        searchbox.send_keys("python selenium tutorial")
        time.sleep(2)
        searchbox.send_keys(Keys.ENTER)
        time.sleep(2)

        l1,l2 = self.get_result(stop)
        titlel1 = pd.DataFrame(l1,columns=['Titles'])
        titlel2 = pd.DataFrame(l2,columns=['Links'])
        read=pd.concat([titlel1,titlel2],axis=1)
        print(read)
        self.driver.close()

